module testbench;

wire [63:0] adder;
wire cout;
wire [63:0] pc;
wire [63:0] ins_memory;
reg clock;

initial begin
		clock = 0;
		forever #5 clock = ~clock;
	end

FullAdder64bit fulladder(.A(pc), .B(64'b100),.Cin(0), adder, .Cout(cout));
pca pca1(.data(adder),.clk(clock), .S(pc), .Cout(cout));
insmemory ins_mem(.address(pc), .out(ins_memory);

endmodule
