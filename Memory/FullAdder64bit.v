module FullAdder64bit(A,B,Cin,S,Cout);
input [64:0] A,B;
input [64:0] Cin;

output[64:0] S;
output [64:0] Cout;

FullAdder16Bit FA_0(A[15:0],B[15:0],Cin[15:0],S[15:0],Cout[15:0]);
FullAdder16Bit FA_1(A[31:16],B[31:16],Cin[31:16],S[31:16],Cout[31:16]);
FullAdder16Bit FA_2(A[57:32],B[57:32],Cin[57:32],S[57:32],Cout[57:32]);
FullAdder16Bit FA_3(A[64:58],B[64:58],Cin[64:58],S[64:58],Cout[64:58]);

endmodule