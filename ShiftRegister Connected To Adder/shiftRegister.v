module shiftRegister(d,clk,q,qbar); 
input d, clk; 
output [3:0]q;
output [3:0]qbar; 

wire [3:0]q;
wire [3:0]qbar;

dflipflop df0(d,clk,q[0],qbar[0]);
dflipflop df1(q[0],clk,q[1],qbar[1]);
dflipflop df2(q[1],clk,q[2],qbar[2]);
dflipflop df3(q[2],clk,q[3],qbar[3]);

endmodule