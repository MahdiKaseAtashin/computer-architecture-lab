module FullAdder16Bit(A,B,Cin,S,Cout);
input[15:0] A,B;
input [15:0]Cin;

output[15:0] S;
output [15:0] Cout;


FullAdder4Bit FA_0(A[3:0],B[3:0],Cin[3:0],S[3:0],Cout[3:0]);
FullAdder4Bit FA_1(A[7:4],B[7:4],Cin[7:4],S[7:4],Cout[7:4]);
FullAdder4Bit FA_2(A[11:8],B[11:8],Cin[11:8],S[11:8],Cout[11:8]);
FullAdder4Bit FA_3(A[15:12],B[15:12],Cin[15:12],S[15:12],Cout[15:12]);

endmodule