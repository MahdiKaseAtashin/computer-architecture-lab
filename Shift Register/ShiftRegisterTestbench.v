
`timescale 1ns / 1ns
module ShiftRegisterTestbench  ; 
 
  reg    d   ; 
  wire  [3:0]  q   ; 
  wire  [3:0]  qbar   ; 
  reg    clk   ; 
  shiftRegister  
   DUT  ( 
       .d (d ) ,
      .q (q ) ,
      .qbar (qbar ) ,
      .clk (clk ) ); 



// "Repeater Pattern" Repeat Forever
// Start Time = 0 ns, End Time = 1 us, Period = 100 ns
  initial
  begin
   repeat(5)
   begin
	    d  = 1'b1  ;
	   #100   d  = 1'b0  ;
	   #100 ;
// 1 us, repeat pattern in loop.
   end
  end


// "Clock Pattern" : dutyCycle = 50
// Start Time = 0 ns, End Time = 1 us, Period = 100 ns
  initial
  begin
   repeat(10)
   begin
	   clk  = 1'b1  ;
	  #50  clk  = 1'b0  ;
	  #50 ;
// 1 us, repeat pattern in loop.
   end
  end

  initial
	#2000 $stop;
endmodule
